﻿namespace DrugsInteraction.Domain.Enums
{
    public enum MedicamentTypes
    {
        ACE_inhibitor = 1,
        Antitrombotik = 2,
        Antidijabetik = 3,
        Antiinfektivni = 4,
        Antipsihotik = 5,
        Antibiotik = 6,
        Antidepresiv = 7,
        Anksiolitik = 8,
        Beta_blokator = 9,
        Diuretik = 10,
        Imunosupresiv = 11,
        Srcani_glikozid = 12,
        NSAIL = 13
    }
}
