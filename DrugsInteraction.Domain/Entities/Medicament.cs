﻿using DrugsInteraction.Domain.Enums;
using System.Collections.Generic;

namespace DrugsInteraction.Domain.Entities
{
    public class Medicament
    {
        public string Name { get; protected set; }
        public MedicamentTypes Type { get; set; }
        public List<string> ActiveSubstances { get; set; }

        public Medicament(string name, MedicamentTypes type, List<string> medSubs)
        {
            Name = name;
            Type = type;
            ActiveSubstances = medSubs;
        }
    }
}
