﻿using DrugsInteraction.Domain.Enums;

namespace DrugsInteraction.Domain.Entities
{
    public class TypeSubstanceInteraction
    {
        public MedicamentTypes MedType { get; protected set; }
        public string Supstance { get; protected set; }
        public string Description { get; protected set; }

        public TypeSubstanceInteraction(MedicamentTypes mt, string sup, string des)
        {
            MedType = mt;
            Supstance = sup;
            Description = des;
        }
    }
}
