﻿using DrugsInteraction.Domain.Entities;
using System.Collections.Generic;

namespace DrugsInteraction.Repository
{
    public class MedicamentRepository
    {
        private static object locker = true;
        private static MedicamentRepository instance;

        public List<Medicament> Medicaments;
        public List<SubSubInteraction> Interactions;
        public List<TypeSubstanceInteraction> TypeSubInteractions;
        public List<TypeTypeInteraction> TypeTypeInteractions;

        protected MedicamentRepository()
        {
            Medicaments = new List<Medicament>();
            Interactions = new List<SubSubInteraction>();
            TypeSubInteractions = new List<TypeSubstanceInteraction>();
            TypeTypeInteractions = new List<TypeTypeInteraction>();
        }

        public static MedicamentRepository Instance
        {
            get
            {
                lock (locker)
                {
                    if (instance == null)
                        instance = new MedicamentRepository();
                }
                return instance;
            }
        }
    }
}
