﻿using DrugsInteraction.Domain.Entities;
using DrugsInteraction.Domain.Enums;
using System.Collections.Generic;

namespace DrugsInteraction.Repository
{
    public static class TestData
    {
        public static List<Medicament> TestMedicaments = new List<Medicament>()
        {
            new Medicament("Brufen",MedicamentTypes.NSAIL, new List<string>() {"ibuprofen"}),
            new Medicament("Aspirin",MedicamentTypes.Antitrombotik, new List<string>() {"acetilsalicilna kiselina"}),
            new Medicament("Humulin",MedicamentTypes.Antidijabetik, new List<string>() {"insulin"}),
            new Medicament("Aglimex",MedicamentTypes.Antidijabetik, new List<string>() {"glimepirid"}),
            new Medicament("Zorkaptil",MedicamentTypes.ACE_inhibitor, new List<string>() {"kaptopril"}),
            new Medicament("Enalapril",MedicamentTypes.ACE_inhibitor, new List<string>() {"enalapril"}),
            new Medicament("Presolol",MedicamentTypes.Beta_blokator, new List<string>() {"metoprolol-tartarat"}),
            new Medicament("Digoxin",MedicamentTypes.Srcani_glikozid, new List<string>() {"digoksin"}),
            new Medicament("Diazepam",MedicamentTypes.Anksiolitik, new List<string>() {"diazepam"}),
            new Medicament("Hermopres",MedicamentTypes.Diuretik, new List<string>() {"amilorid", "hidrohlortiazid"}),
            new Medicament("Sandimmun Neoral",MedicamentTypes.Imunosupresiv, new List<string>() {"ciklosporin"}),
            new Medicament("Eritromicin",MedicamentTypes.Antibiotik, new List<string>() {"eritromicin"}),
            new Medicament("Flunisan",MedicamentTypes.Antidepresiv, new List<string>() {"fluoksetin"})
        };

        public static List<SubSubInteraction> TestSubSubInteractions = new List<SubSubInteraction>()
        {
            new SubSubInteraction("ibuprofen","acetilsalicilna kiselina","Ibuprofen povećava toksičnost aspirina što uzrokuje razvoj čira na gastrointestinalnom traktu,krvarenje ili modrice. Ibuprofen smanjuje antiagregacione efekte aspirina."),
            new SubSubInteraction("ibuprofen","ciklosporin","Kombinacija ova 2 leka može biti štetna za bubrege. Neki od simptoma oštećenja bubrega su: osećaj nedostatka daha, pospanost, pojačana žeđ, mučnina, mokrenje manje ili više nego inače."),
            new SubSubInteraction("ibuprofen","amilorid","Kombinacija ova 2 leka dovodi do povećanog nivoa kalijuma u krvi. Takođe može uticati na krvni pritisak i rad bubrega."),
            new SubSubInteraction("acetilsalicilna kiselina","digoksin","Kombinacija ova 2 leka dovodi do povećanog efekta digoksina koji usporava srčani ritam i povećava snagu srca za vreme otkucaja. Takođe, povećana je koncentracija kalijuma u krvi."),
            new SubSubInteraction("insulin","fluoksetin","Upotreba Flunisana i sličnih antidepresiva sa insulinom povećati rizik od hipoglikemije ili niskog nivoa šećera u krvi."),
            new SubSubInteraction("insulin","glimepirid","Istovremena uotreba ova dva antidijabetika može pojačati rizik od hipoglikemije, posebno kod starijh i neuhranjenih pacijenata kao i kod onih koji imaju insuficijenciju nadbubrežne žlezde. Potrebno je pažljivo praćenje glukoze u krvi, a često i manja doza jednog od ova 2 leka."),
            new SubSubInteraction("digoksin","eritromicin","Ukoliko ove lekove uzimamo oralno eritromicin će povećati nivo ii efekat digoksina suzbijanjem bakterija u stomaku koje mogu pomoći apsorpciji leka. Eritromicin utiče na to kako se digoksin eliminiše iz organizma."),
            new SubSubInteraction("digoksin","ciklosporin","Ciklosporin povećava nivo digoksina tako što utiče na eliminaciju leka iz organizma."),
            new SubSubInteraction("diazepam","fluoksetin","Fluoksetin će povećati nivo ili efekat diazepama menjanjem metabolizma leka."),
            new SubSubInteraction("ciklosporin","eritromicin","Ukoliko ove lekove uzimamo oralno eritromicin će povećati nivo ii efekat ciklosporina. Eritromicin utiče na to kako se ciklosporin eliminiše iz organizma.")
        };

        public static List<TypeSubstanceInteraction> TestTypeSubInteractions = new List<TypeSubstanceInteraction>()
        {
            new TypeSubstanceInteraction(MedicamentTypes.ACE_inhibitor,"ibuprofen","Kombinacija ova 2 leka dovodi do smanjenog efekta ACE inhibitora u snižavanju krvnog pritiska. Takođe može smanjiti rad bubrega, naročito kod starijih osoba."),
            new TypeSubstanceInteraction(MedicamentTypes.Beta_blokator,"ibuprofen","Kombinacija ova 2 leka dovodi do smanjenog efekta Presolola u snižavanju krvnog pritiska, posebno ako često koristite ibuprofen(npr za lečenje artritisa ili hroničnog bola). Takođe može doći do povećanog nivoa kalijuma u krvi."),
            new TypeSubstanceInteraction(MedicamentTypes.Antidijabetik,"ibuprofen","Ibuprofen pojačava hipoglikemijsko dejstvo(smanjenje glukoze u krvi) oralnih antidijabetika i insulina, pa može biti potrebno korigovanje njihovog doziranja."),
            new TypeSubstanceInteraction(MedicamentTypes.Diuretik,"ibuprofen","Ibuprofen može smanjiti efekat diuretika. Diuretici, takođe, mogu povećati rizik pojave nefrotoksičnosti koju izaziva ibuprofen."),
            new TypeSubstanceInteraction(MedicamentTypes.Antidepresiv,"ibuprofen","Ibuprofen nikako ne bi trebali uzimati u kombinaciji sa antidepresivima. Može doći do povećanja toksičnosti ovih lekova, kao i krvarenja gornjeg dela digestivnog trakta."),
            new TypeSubstanceInteraction(MedicamentTypes.Antidijabetik,"acetilsalicilna kiselina","Aspirin pojačava hipoglikemijsko dejstvo(smanjenje glukoze u krvi) oralnih antidijabetika i insulina, pa može biti potrebno korigovanje njihovog doziranja."),
            new TypeSubstanceInteraction(MedicamentTypes.Diuretik,"acetilsalicilna kiselina","Aspirin može smanjiti efekat diuretika."),
            new TypeSubstanceInteraction(MedicamentTypes.ACE_inhibitor,"acetilsalicilna kiselina","Kombinacija ova 2 leka dovodi do smanjenog efekta ACE inhibitora u snižavanju krvnog pritiska. Takođe može smanjiti rad bubrega, naročito kod starijih osoba."),
            new TypeSubstanceInteraction(MedicamentTypes.Antidepresiv,"acetilsalicilna kiselina","Aspirin nikako ne bi trebali uzimati u kombinaciji sa antidepresivima. Može doći do povećanja toksičnosti ovih lekova, kao i krvarenja gornjeg dela digestivnog trakta."),
            new TypeSubstanceInteraction(MedicamentTypes.ACE_inhibitor,"amilorid","Upotreba ACE inhibitora zajedno sa amiloridom može povećati nivo kalijuma u krvi(hiperkalijemija), posebno ako ste dehidrirani ili imate bubrežne bolesti, dijabetes ili zatezanje srca."),
            new TypeSubstanceInteraction(MedicamentTypes.ACE_inhibitor,"hidrohlortiazid","ACE inhibitori i hidrohloriazid često se kombinuju zajedno, međutim njihovi efekti mogu biti aditivni na snižavanje krvnog pritiska."),
            new TypeSubstanceInteraction(MedicamentTypes.ACE_inhibitor,"ciklosporin","Upotreba ACE inhibitora sa ciklosporinom može povećati nivo kalijuma u krvi, što dalje može da dovede do hiperkalijemije, koja u težim slučajevima može da dovede do insuficijencije bubrega, paralize mišića, nepravilnog srčanog ritma i zastoja srca."),
            new TypeSubstanceInteraction(MedicamentTypes.Beta_blokator,"digoksin","Digoksin povećava toksičnost beta-blokatora pa dolazi do povećang rizika od bradikardije(nizak broj otkucaja srca). Takođe, beta-blokator povećava efekat digoksina što usporava rad srca."),
            new TypeSubstanceInteraction(MedicamentTypes.Diuretik,"ciklosporin","Kombinacija ova dva leka može izazvati povećan nivo kalijuma u krvi. Takođe postoji rizik od niskog natrijuma u krvi(hiponatrijemija).")
        };

        public static List<TypeTypeInteraction> TestTypeTypeInteractions = new List<TypeTypeInteraction>()
        {
            new TypeTypeInteraction(MedicamentTypes.Beta_blokator,MedicamentTypes.Antidijabetik,"Beta blokatori, poput Presolola mogu povećati rizik, ozbiljnost i/ili trajanje hipoglikemije(nizak nivo šećera u krvi) kod pacijenata koji primaju insulin i neke druge antidijabetičke lekove. Takođe, beta blokatori mogu maskirati neke od simptoma hipoglikemije, kao što je ubrzan rad srca, što otežava njeno prepoznavanje."),
            new TypeTypeInteraction(MedicamentTypes.Antidijabetik,MedicamentTypes.Diuretik,"Tiazidni diuretici mogu uticati na toleranciju na glukozu i mogu dovesti do pogoršanja dijabetes melitusa, zato treba izbegavati njihovu primenu kod dijabetičara. Može biti potrebno korigovanje doziranja antidijabetičkih lekova, uključujući i insulin."),
            new TypeTypeInteraction(MedicamentTypes.Antidijabetik,MedicamentTypes.ACE_inhibitor,"Upotreba ACE inhibitora sa insulinom ili drugim antidijabeticima može povećati rizik od hipoglikemije ili niskog nivoa šećera u krvi.")
        };
    }
}
