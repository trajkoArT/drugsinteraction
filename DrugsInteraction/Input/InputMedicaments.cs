﻿using DrugsInteraction.Domain.Entities;

namespace DrugsInteraction.Input
{
    public class InputMedicaments
    {
        public Medicament medicament1;
        public Medicament medicament2;

        public InputMedicaments(Medicament med1, Medicament med2)
        {
            medicament1 = med1;
            medicament2 = med2;
        }
    }
}
