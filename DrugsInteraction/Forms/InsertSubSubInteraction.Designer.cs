﻿namespace DrugsInteraction
{
    partial class InsertInteraction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtSub1 = new System.Windows.Forms.TextBox();
            this.txtSub2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtInteraction = new System.Windows.Forms.TextBox();
            this.btnAddInteraction = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Substance 1:";
            // 
            // txtSub1
            // 
            this.txtSub1.Location = new System.Drawing.Point(115, 45);
            this.txtSub1.Name = "txtSub1";
            this.txtSub1.Size = new System.Drawing.Size(213, 20);
            this.txtSub1.TabIndex = 1;
            // 
            // txtSub2
            // 
            this.txtSub2.Location = new System.Drawing.Point(115, 83);
            this.txtSub2.Name = "txtSub2";
            this.txtSub2.Size = new System.Drawing.Size(213, 20);
            this.txtSub2.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Substance 2:";
            // 
            // txtInteraction
            // 
            this.txtInteraction.Location = new System.Drawing.Point(42, 148);
            this.txtInteraction.Multiline = true;
            this.txtInteraction.Name = "txtInteraction";
            this.txtInteraction.Size = new System.Drawing.Size(286, 205);
            this.txtInteraction.TabIndex = 4;
            // 
            // btnAddInteraction
            // 
            this.btnAddInteraction.Location = new System.Drawing.Point(181, 383);
            this.btnAddInteraction.Name = "btnAddInteraction";
            this.btnAddInteraction.Size = new System.Drawing.Size(147, 28);
            this.btnAddInteraction.TabIndex = 5;
            this.btnAddInteraction.Text = "Add Interaction";
            this.btnAddInteraction.UseVisualStyleBackColor = true;
            this.btnAddInteraction.Click += new System.EventHandler(this.btnAddInteraction_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 124);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Description of interaction:";
            // 
            // InsertInteraction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(375, 428);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnAddInteraction);
            this.Controls.Add(this.txtInteraction);
            this.Controls.Add(this.txtSub2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtSub1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InsertInteraction";
            this.Text = "InsertInteraction";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSub1;
        private System.Windows.Forms.TextBox txtSub2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtInteraction;
        private System.Windows.Forms.Button btnAddInteraction;
        private System.Windows.Forms.Label label3;
    }
}