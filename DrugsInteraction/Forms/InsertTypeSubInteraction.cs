﻿using DrugsInteraction.Domain.Entities;
using DrugsInteraction.Domain.Enums;
using DrugsInteraction.Repository;
using System;
using System.Windows.Forms;

namespace DrugsInteraction
{
    public partial class InsertTypeSubInteraction : Form
    {
        public InsertTypeSubInteraction()
        {
            InitializeComponent();
        }

        private void InsertTypeInteraction_Load(object sender, EventArgs e)
        {
            cmbxType.DataSource = Enum.GetValues(typeof(MedicamentTypes));
            cmbxType.SelectedItem = null;
        }

        private void btnAddInteraction_Click(object sender, EventArgs e)
        {
            if (checkBoxes())
            {
                MedicamentTypes type;
                Enum.TryParse<MedicamentTypes>(cmbxType.SelectedValue.ToString(), out type);
                MedicamentRepository.Instance.TypeSubInteractions.Add(new TypeSubstanceInteraction(type, txtSub.Text.ToLower(), txtInteraction.Text));
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private bool checkBoxes()
        {
            if (cmbxType.SelectedItem != null && txtSub.Text != "" && txtInteraction.Text != "")
                return true;
            else
                MessageBox.Show("All textboxes must be filled.", "Warrning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            return false;
        }
    }
}
