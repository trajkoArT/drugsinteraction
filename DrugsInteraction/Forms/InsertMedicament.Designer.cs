﻿namespace DrugsInteraction
{
    partial class InsertMedicament
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtActiveSubstance = new System.Windows.Forms.TextBox();
            this.btnAddSub = new System.Windows.Forms.Button();
            this.btnAddMed = new System.Windows.Forms.Button();
            this.lbxActive = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbxType = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name of medicament:";
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.Location = new System.Drawing.Point(149, 32);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(245, 20);
            this.txtName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Active Supstances:";
            // 
            // txtActiveSubstance
            // 
            this.txtActiveSubstance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtActiveSubstance.Location = new System.Drawing.Point(149, 102);
            this.txtActiveSubstance.Name = "txtActiveSubstance";
            this.txtActiveSubstance.Size = new System.Drawing.Size(245, 20);
            this.txtActiveSubstance.TabIndex = 3;
            // 
            // btnAddSub
            // 
            this.btnAddSub.Location = new System.Drawing.Point(208, 138);
            this.btnAddSub.Name = "btnAddSub";
            this.btnAddSub.Size = new System.Drawing.Size(116, 23);
            this.btnAddSub.TabIndex = 4;
            this.btnAddSub.Text = "Add supstance";
            this.btnAddSub.UseVisualStyleBackColor = true;
            this.btnAddSub.Click += new System.EventHandler(this.btnAddSub_Click);
            // 
            // btnAddMed
            // 
            this.btnAddMed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddMed.Location = new System.Drawing.Point(259, 278);
            this.btnAddMed.Name = "btnAddMed";
            this.btnAddMed.Size = new System.Drawing.Size(135, 23);
            this.btnAddMed.TabIndex = 5;
            this.btnAddMed.Text = "Add medicament";
            this.btnAddMed.UseVisualStyleBackColor = true;
            this.btnAddMed.Click += new System.EventHandler(this.btnAddMed_Click);
            // 
            // lbxActive
            // 
            this.lbxActive.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbxActive.FormattingEnabled = true;
            this.lbxActive.Location = new System.Drawing.Point(149, 178);
            this.lbxActive.Name = "lbxActive";
            this.lbxActive.Size = new System.Drawing.Size(245, 82);
            this.lbxActive.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Type of medicament:";
            // 
            // cmbxType
            // 
            this.cmbxType.FormattingEnabled = true;
            this.cmbxType.Location = new System.Drawing.Point(149, 67);
            this.cmbxType.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmbxType.Name = "cmbxType";
            this.cmbxType.Size = new System.Drawing.Size(245, 21);
            this.cmbxType.TabIndex = 8;
            // 
            // InsertMedicament
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 317);
            this.Controls.Add(this.cmbxType);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbxActive);
            this.Controls.Add(this.btnAddMed);
            this.Controls.Add(this.btnAddSub);
            this.Controls.Add(this.txtActiveSubstance);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InsertMedicament";
            this.Text = "Insert Medicament";
            this.Load += new System.EventHandler(this.InsertMedicament_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtActiveSubstance;
        private System.Windows.Forms.Button btnAddSub;
        private System.Windows.Forms.Button btnAddMed;
        private System.Windows.Forms.ListBox lbxActive;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbxType;
    }
}