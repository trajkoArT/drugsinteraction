﻿namespace DrugsInteraction
{
    partial class InsertTypeTypeInteraction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbxType1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnAddInteraction = new System.Windows.Forms.Button();
            this.txtInteraction = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbxType2 = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // cmbxType1
            // 
            this.cmbxType1.FormattingEnabled = true;
            this.cmbxType1.Location = new System.Drawing.Point(181, 41);
            this.cmbxType1.Name = "cmbxType1";
            this.cmbxType1.Size = new System.Drawing.Size(225, 24);
            this.cmbxType1.TabIndex = 21;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(169, 17);
            this.label3.TabIndex = 20;
            this.label3.Text = "Description of interaction:";
            // 
            // btnAddInteraction
            // 
            this.btnAddInteraction.Location = new System.Drawing.Point(211, 443);
            this.btnAddInteraction.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddInteraction.Name = "btnAddInteraction";
            this.btnAddInteraction.Size = new System.Drawing.Size(196, 34);
            this.btnAddInteraction.TabIndex = 19;
            this.btnAddInteraction.Text = "Add Interaction";
            this.btnAddInteraction.UseVisualStyleBackColor = true;
            this.btnAddInteraction.Click += new System.EventHandler(this.btnAddInteraction_Click);
            // 
            // txtInteraction
            // 
            this.txtInteraction.Location = new System.Drawing.Point(26, 162);
            this.txtInteraction.Margin = new System.Windows.Forms.Padding(4);
            this.txtInteraction.Multiline = true;
            this.txtInteraction.Name = "txtInteraction";
            this.txtInteraction.Size = new System.Drawing.Size(380, 251);
            this.txtInteraction.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 44);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 17);
            this.label1.TabIndex = 15;
            this.label1.Text = "Type of medicament 1:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 85);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 17);
            this.label4.TabIndex = 22;
            this.label4.Text = "Type of medicament 2:";
            // 
            // cmbxType2
            // 
            this.cmbxType2.FormattingEnabled = true;
            this.cmbxType2.Location = new System.Drawing.Point(181, 82);
            this.cmbxType2.Name = "cmbxType2";
            this.cmbxType2.Size = new System.Drawing.Size(224, 24);
            this.cmbxType2.TabIndex = 23;
            // 
            // InsertTypeTypeInteraction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 504);
            this.Controls.Add(this.cmbxType2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbxType1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnAddInteraction);
            this.Controls.Add(this.txtInteraction);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InsertTypeTypeInteraction";
            this.Text = "Insert Type Type Interaction";
            this.Load += new System.EventHandler(this.InsertTypeTypeInteraction_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbxType1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnAddInteraction;
        private System.Windows.Forms.TextBox txtInteraction;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbxType2;
    }
}
