﻿namespace DrugsInteraction
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insertNewMedicamentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insertNewInteractionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insertNewTypesupstanceInteractionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insertNewTypetypeInteractionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmbxMed1 = new System.Windows.Forms.ComboBox();
            this.cmbxMed2 = new System.Windows.Forms.ComboBox();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.btnCheck = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(509, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.insertNewMedicamentToolStripMenuItem,
            this.insertNewInteractionToolStripMenuItem,
            this.insertNewTypesupstanceInteractionToolStripMenuItem,
            this.insertNewTypetypeInteractionToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(75, 24);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // insertNewMedicamentToolStripMenuItem
            // 
            this.insertNewMedicamentToolStripMenuItem.Name = "insertNewMedicamentToolStripMenuItem";
            this.insertNewMedicamentToolStripMenuItem.Size = new System.Drawing.Size(374, 26);
            this.insertNewMedicamentToolStripMenuItem.Text = "Insert new medicament";
            this.insertNewMedicamentToolStripMenuItem.Click += new System.EventHandler(this.insertNewMedicamentToolStripMenuItem_Click);
            // 
            // insertNewInteractionToolStripMenuItem
            // 
            this.insertNewInteractionToolStripMenuItem.Name = "insertNewInteractionToolStripMenuItem";
            this.insertNewInteractionToolStripMenuItem.Size = new System.Drawing.Size(374, 26);
            this.insertNewInteractionToolStripMenuItem.Text = "Insert new supstance-supstance interaction";
            this.insertNewInteractionToolStripMenuItem.Click += new System.EventHandler(this.insertNewInteractionToolStripMenuItem_Click);
            // 
            // insertNewTypesupstanceInteractionToolStripMenuItem
            // 
            this.insertNewTypesupstanceInteractionToolStripMenuItem.Name = "insertNewTypesupstanceInteractionToolStripMenuItem";
            this.insertNewTypesupstanceInteractionToolStripMenuItem.Size = new System.Drawing.Size(374, 26);
            this.insertNewTypesupstanceInteractionToolStripMenuItem.Text = "Insert new type-supstance interaction";
            this.insertNewTypesupstanceInteractionToolStripMenuItem.Click += new System.EventHandler(this.insertNewTypesupstanceInteractionToolStripMenuItem_Click);
            // 
            // insertNewTypetypeInteractionToolStripMenuItem
            // 
            this.insertNewTypetypeInteractionToolStripMenuItem.Name = "insertNewTypetypeInteractionToolStripMenuItem";
            this.insertNewTypetypeInteractionToolStripMenuItem.Size = new System.Drawing.Size(374, 26);
            this.insertNewTypetypeInteractionToolStripMenuItem.Text = "Insert new type-type interaction";
            this.insertNewTypetypeInteractionToolStripMenuItem.Click += new System.EventHandler(this.insertNewTypetypeInteractionToolStripMenuItem_Click);
            // 
            // cmbxMed1
            // 
            this.cmbxMed1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbxMed1.FormattingEnabled = true;
            this.cmbxMed1.Location = new System.Drawing.Point(137, 53);
            this.cmbxMed1.Margin = new System.Windows.Forms.Padding(4);
            this.cmbxMed1.Name = "cmbxMed1";
            this.cmbxMed1.Size = new System.Drawing.Size(339, 24);
            this.cmbxMed1.TabIndex = 1;
            // 
            // cmbxMed2
            // 
            this.cmbxMed2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbxMed2.FormattingEnabled = true;
            this.cmbxMed2.Location = new System.Drawing.Point(137, 112);
            this.cmbxMed2.Margin = new System.Windows.Forms.Padding(4);
            this.cmbxMed2.Name = "cmbxMed2";
            this.cmbxMed2.Size = new System.Drawing.Size(339, 24);
            this.cmbxMed2.TabIndex = 2;
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Location = new System.Drawing.Point(21, 57);
            this.lbl1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(100, 17);
            this.lbl1.TabIndex = 3;
            this.lbl1.Text = "Medicament 1:";
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.Location = new System.Drawing.Point(21, 116);
            this.lbl2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(100, 17);
            this.lbl2.TabIndex = 4;
            this.lbl2.Text = "Medicament 2:";
            // 
            // btnCheck
            // 
            this.btnCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCheck.Location = new System.Drawing.Point(324, 175);
            this.btnCheck.Margin = new System.Windows.Forms.Padding(4);
            this.btnCheck.Name = "btnCheck";
            this.btnCheck.Size = new System.Drawing.Size(153, 33);
            this.btnCheck.TabIndex = 5;
            this.btnCheck.Text = "Check Interaction";
            this.btnCheck.UseVisualStyleBackColor = true;
            this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(509, 240);
            this.Controls.Add(this.btnCheck);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.cmbxMed1);
            this.Controls.Add(this.cmbxMed2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "Drugs interaction";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem insertNewMedicamentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem insertNewInteractionToolStripMenuItem;
        private System.Windows.Forms.ComboBox cmbxMed1;
        private System.Windows.Forms.ComboBox cmbxMed2;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Button btnCheck;
        private System.Windows.Forms.ToolStripMenuItem insertNewTypesupstanceInteractionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem insertNewTypetypeInteractionToolStripMenuItem;
    }
}
