﻿using DrugsInteraction.Domain.Entities;
using DrugsInteraction.Input;
using DrugsInteraction.Repository;
using NRules;
using NRules.Fluent;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;

namespace DrugsInteraction
{
    public partial class MainForm : Form
    {
        private RuleRepository _repository;
        private ISessionFactory _factory;
        public MainForm()
        {
            InitializeComponent();
            _repository = new RuleRepository();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MedicamentRepository.Instance.Medicaments = TestData.TestMedicaments;
            MedicamentRepository.Instance.Interactions = TestData.TestSubSubInteractions;
            MedicamentRepository.Instance.TypeSubInteractions = TestData.TestTypeSubInteractions;
            MedicamentRepository.Instance.TypeTypeInteractions = TestData.TestTypeTypeInteractions;

            BindMedicamentList(cmbxMed1, MedicamentRepository.Instance.Medicaments);
            cmbxMed2.BindingContext = new BindingContext();
            BindMedicamentList(cmbxMed2, MedicamentRepository.Instance.Medicaments);

            _repository.Load(x => x
                    .From(Assembly.GetExecutingAssembly())
                    .Where(rule => rule.IsTagged("Interaction")));

            _factory = _repository.Compile();
        }

        private void insertNewMedicamentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form insert = new InsertMedicament();
            insert.ShowDialog();

            if (insert.DialogResult == DialogResult.OK)
            {
                cmbxMed1.DataSource = null;
                BindMedicamentList(cmbxMed1, MedicamentRepository.Instance.Medicaments);
                cmbxMed2.DataSource = null;
                BindMedicamentList(cmbxMed2, MedicamentRepository.Instance.Medicaments);
            }
        }
        private void insertNewInteractionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form insert = new InsertInteraction();
            insert.ShowDialog();
        }

        private void insertNewTypesupstanceInteractionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form typeInteraction = new InsertTypeSubInteraction();
            typeInteraction.ShowDialog();
        }

        private void insertNewTypetypeInteractionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form typeTypeInteraction = new InsertTypeTypeInteraction();
            typeTypeInteraction.ShowDialog();
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            if (CheckBoxes())
            {
                var session = _factory.CreateSession();

                InputMedicaments input = new InputMedicaments((Medicament)cmbxMed1.SelectedItem, (Medicament)cmbxMed2.SelectedItem);

                session.Insert(input);
                session.InsertAll(MedicamentRepository.Instance.Interactions);
                session.InsertAll(MedicamentRepository.Instance.TypeSubInteractions);
                session.InsertAll(MedicamentRepository.Instance.TypeTypeInteractions);

                if (session.Fire() == 0)
                    MessageBox.Show("There is no interactions between these two medicaments.",
                        "No interaction", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private bool CheckBoxes()
        {
            if (cmbxMed1.SelectedItem != null && cmbxMed2.SelectedItem != null)
            {
                if (cmbxMed1.SelectedItem != cmbxMed2.SelectedItem)
                    return true;
                else
                    MessageBox.Show("Select different medicaments!", "Warrning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
                MessageBox.Show("Choose medicamention names from list!", "Warrning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            return false;
        }

        private void BindMedicamentList(ComboBox cmbx, List<Medicament> meds)
        {
            cmbx.ValueMember = "Name";
            cmbx.DisplayMember = "Name";
            cmbx.DataSource = meds;
            cmbx.SelectedItem = null;
        }
    }
}
