﻿using DrugsInteraction.Domain.Entities;
using DrugsInteraction.Input;
using NRules.Fluent.Dsl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace DrugsInteraction.Rules
{
    [Tag("Interaction")]
    public class TypeSubstanceInteractionRule : Rule
    {
        public override void Define()
        {
            InputMedicaments input = null;
            IEnumerable<TypeSubstanceInteraction> typeInteractions = null;

            When()
                .Match<InputMedicaments>(() => input, im => im.medicament1.ActiveSubstances.Count > 0 && im.medicament2.ActiveSubstances.Count > 0)
                .Query(() => typeInteractions, x => x
                    .Match<TypeSubstanceInteraction>(
                        i => (input.medicament1.Type == i.MedType &&
                             input.medicament2.ActiveSubstances.Contains(i.Supstance))
                             ||
                             (input.medicament1.ActiveSubstances.Contains(i.Supstance) &&
                             input.medicament2.Type == i.MedType)
                    )
                    .Collect()
                    .Where(c => c.Any()));
            Then()
                .Do(ctx => DisplayTypeInteraction(typeInteractions));
        }

        public static void DisplayTypeInteraction(IEnumerable<TypeSubstanceInteraction> typeInteractions)
        {
            string allInteractions = "";

            foreach (var inter in typeInteractions)
            {
                allInteractions += "- " + inter.Description + Environment.NewLine;
            }

            MessageBox.Show(allInteractions, "Interactions between selected medicaments (type-sub)");
        }
    }
}
