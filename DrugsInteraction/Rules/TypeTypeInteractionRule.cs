﻿using DrugsInteraction.Domain.Entities;
using DrugsInteraction.Input;
using NRules.Fluent.Dsl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace DrugsInteraction.Rules
{
    [Tag("Interaction")]
    public class TypeTypeInteractionRule : Rule
    {
        public override void Define()
        {
            InputMedicaments input = null;
            IEnumerable<TypeTypeInteraction> typeTypeInteractions = null;

            When()
                .Match<InputMedicaments>(() => input)
                .Query(() => typeTypeInteractions, x => x
                    .Match<TypeTypeInteraction>(
                        i => (input.medicament1.Type == i.MedType1 &&
                             input.medicament2.Type == i.MedType2)
                             ||
                             (input.medicament1.Type == i.MedType2 &&
                             input.medicament2.Type == i.MedType1)
                    )
                    .Collect()
                    .Where(c => c.Any()));
            Then()
                .Do(ctx => DisplayTypeInteraction(typeTypeInteractions));
        }

        public static void DisplayTypeInteraction(IEnumerable<TypeTypeInteraction> interactions)
        {
            string allInteractions = "";

            foreach (var inter in interactions)
            {
                allInteractions += "- " + inter.Description + Environment.NewLine;
            }

            MessageBox.Show(allInteractions, "Interactions between selected medicaments (type-type)");
        }
    }
}
