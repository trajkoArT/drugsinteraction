﻿using DrugsInteraction.Domain.Entities;
using DrugsInteraction.Input;
using NRules.Fluent.Dsl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace DrugsInteraction.Rules
{
    [Tag("Interaction")]
    public class SubSubInteracitonRule : Rule
    {
        public override void Define()
        {
            InputMedicaments input = null;
            IEnumerable<SubSubInteraction> interactions = null;

            When()
                .Match<InputMedicaments>(() => input, im => im.medicament1.ActiveSubstances.Count > 0 && im.medicament2.ActiveSubstances.Count > 0)
                .Query(() => interactions, x => x
                    .Match<SubSubInteraction>(
                        i => (input.medicament1.ActiveSubstances.Contains(i.ActiveSubstance1) &&
                             input.medicament2.ActiveSubstances.Contains(i.ActiveSubstance2))
                             ||
                             (input.medicament1.ActiveSubstances.Contains(i.ActiveSubstance2) &&
                             input.medicament2.ActiveSubstances.Contains(i.ActiveSubstance1))
                    )
                    .Collect()
                    .Where(c => c.Any()));
            Then()
                .Do(ctx => DisplayInteraction(interactions));
        }

        public static void DisplayInteraction(IEnumerable<SubSubInteraction> interactions)
        {
            string allInteractions = "";

            foreach (var inter in interactions)
            {
                allInteractions += "- " + inter.Description + Environment.NewLine;
            }

            MessageBox.Show(allInteractions, "Interactions between selected medicaments (sub-sub)");
        }
    }
}
